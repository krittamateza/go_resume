package utils

import (
	"crypto/tls"
	"log"

	"gopkg.in/gomail.v2"
)

func SendMail(){
    m := gomail.NewMessage()

    m.SetHeader("Subject", "Test Mail")
	m.SetAddressHeader("From", "noreply@quulio.com", "noreply@quulio.com")
	m.SetHeader("To", "krittamateza@gmail.com")
	m.SetBody("text/plain", "Test Success")

    d := gomail.NewDialer("smtp.gmail.com", 587, "6331305040@lamduan.mfu.ac.th", "6031305002Za")
    d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
    if err := d.DialAndSend(m); err != nil {
		log.Println(err)
	}
}